(ns main.core
  (:require [electron :refer [app BrowserWindow]]))

(defonce main-window (atom nil))

(defn init-browser []
  (reset! main-window (BrowserWindow.
                       #js {:width 800
                            :height 600
                            :webPreferences #js {:contextIsolation true
                                                 :preload (str "file://" js/__dirname "/preload.js")}}))
  (.loadURL @main-window (str "file://" js/__dirname "/index.html"))
  (.on @main-window "closed" #(reset! main-window nil)))

(defn main []
  (.on app "ready" init-browser)
  (.on app "window-all-closed" (fn [] (.quit app))))
