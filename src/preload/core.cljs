(ns preload.core
  (:require [electron :refer [contextBridge ipcRenderer]]))

(def methods 
  #js {:send-ipc (fn [event-name data] (.send ipcRenderer event-name data))
       :on-ipc (fn [event-name handler] (.on ipcRenderer event-name handler))})

(defn main []
  (.exposeInMainWorld contextBridge "api" methods))
