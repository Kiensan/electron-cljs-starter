(ns renderer.core
  (:require [goog.dom :as gdom]
            [rum.core :as rum]))

(rum/defc root []
  [:div
   [:h1 "Hello World"]])

(defn mount []
  (when-let [el (gdom/getElement "app")]
    (rum/mount (root) el)))

(defn start! []
  (mount))
