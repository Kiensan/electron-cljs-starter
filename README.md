# electron-cljs-starter
Electron + ClojureScript + shadow-cljs + Rum

## How to Run
```
npm install
npm run build
npm run start
```